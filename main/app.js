function addTokens(input, tokens){

    if(typeof input !== 'string') {
        throw Error('Invalid input');
    }

    if(input.length < 6) {
        throw Error('Input should have at least 6 characters');
    }

    for(let element in tokens) {
        if(typeof tokens[element]['tokenName'] !== 'string') {
            throw Error('Invalid array format');
        }
    }

    if(input.indexOf('...') === -1) {
        return input;
    }

    let newString = '';
    for(let element in tokens) {
        if(tokens[element].hasOwnProperty("tokenName")) {
            newString = input.replace('...', '${'+tokens[element]['tokenName']+'}');
        }
    }

    if(newString.length > 0) {
        return newString;
    }
}

const app = {
    addTokens: addTokens
}

module.exports = app;